// ==UserScript==
// @name Чатвтроём
// @namespace CVT2
// @include https://chatvdvoem.ru/*
// @version 0.2.9
// @grant none
// @description
// @downloadURL https://bitbucket.org/2ch_dev/cvd_double/raw/master/cvt2.user.js
// @encoding utf-8
// ==/UserScript==

function inject(fn) {
	source = '(' + fn + ')();';

	var script = document.createElement('script');
	script.setAttribute("type", "application/javascript");
	script.textContent = source;
	document.body.appendChild(script);
}

inject(function() {

	function log(msg) {
		if (window.cvtSettings.debugLogging) {
			console.log(msg);
		}
	}

	function injectToFrame(fn, frame) {
		source = fn.toString();

		var script = frame.document.createElement('script');
		script.setAttribute("type", "application/javascript");
		script.textContent = source;
		frame.document.body.appendChild(script);
	}


	function haveContacts(text) {
		var digts = "0123456789";
		function isDigit(ch) {
			return digts.indexOf(ch) != -1;
		}
		var iend = text.length;
		var lastDigit = 'n';
		var ctr = 0;
		for (var i = 0; i < iend; ++i) {
			if (isDigit(text[i]) && lastDigit != 'n' && lastDigit != text[i]) {
				ctr += 1;
			}
			if (isDigit(text[i])) {
				lastDigit = text[i];
			} else {
				lastDigit = 'n';
			}
		}
		if (ctr > 6) return true;
		return false;
	}
	
	function createCookie(name, value, days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			var expires = '; expires=' + date.toGMTString();
		} 
		else expires = '';
		document.cookie = name + '=' + value + expires + '; path=/';
	}

	$('.startChat').append('<a href="#">Траллеть</a>').click(function() {

		var chatOriginalHTML = document.documentElement.innerHTML;
		
		var frameset =
			'<frameset cols="50%, 50$">'+
				'<iframe width="49%" height="99%" id="frame1"></iframe>'+
				'<iframe width="50%" height="99%" id="frame2"></iframe>'+
			'</frameset>';

		$('body').empty().append(frameset);
		
		function loadFrame(frame) {
			var frameDocument = frame.document;
			if (frame.contentDocument)
			 frameDocument = frame.contentDocument;
			else if (newFrame.contentWindow)
			 frameDocument = frame.contentWindow.document;

			frameDocument.open();
			frameDocument.writeln(chatOriginalHTML);
			frameDocument.close();
		}
		
		loadFrame(document.getElementById("frame1"));
		loadFrame(document.getElementById("frame2"));
		
		setTimeout(function() {
			var settings = window.cvtSettings = new Settings();
			var bridge = window.cvtBridge = new Bridge(window.frames[0], window.frames[1]);
			settings.addButton('Рестарт', function() { bridge.restartChats(); });
			settings.addButton('Смотреть лог', function() { bridge.showLog(); });
		}, 2000);

	});

	function Bridge(one, two) {
		this.log = '';
		this.chats = [new Chat(0, one, this), new Chat(1, two, this)];
		this.startChats();
	}

	Bridge.prototype.startChats = function() {
		if (this.startChatsLock) return;
		this.startChatsLock = true;
		this.chats[0].start();
		setTimeout(function() {
			this.chats[1].start();
			this.startChatsLock = false;
		}.bind(this), 400);
	}

	Bridge.prototype.restartChats = function() {
		this.chats[0].stop('program');
		this.chats[1].stop('program');
		if (!window.cvtSettings.autoRestart) {
			setTimeout(function() {
				this.chats[0].start();
			}.bind(this), 400);
			setTimeout(function() {
				this.chats[1].start();
			}.bind(this), 800);
		}
	}

	Bridge.prototype.onChatConnected = function(chat) {
		this.resetProtractedDialogTimer();
		this.messageCounter(0);
	}

	Bridge.prototype.clearProtractedDialogTimer = function() {
		clearTimeout(this.protractedDialogTimer);
		this.protractedDialogTimer = null;
	}

	Bridge.prototype.resetProtractedDialogTimer = function() {
		this.clearProtractedDialogTimer();
		if (window.cvtSettings.restartProtractedDialogs && this.messageCounter() < 30) {
			this.protractedDialogTimer = setTimeout(function() {
				log('Restart chats, protracted dialog');
				this.restartChats();
			}.bind(this), 4 * 60 * 1000);
		}
	}

	Bridge.prototype.messageCounter = function(val) {
		if (val !== undefined) {
			this._messageCounter = val;
		}
		return this._messageCounter;
	}

	Bridge.prototype.incrementMessageCounter = function() {
		this.messageCounter(this.messageCounter() + 1);
	}

	Bridge.prototype.onMessage = function(message) {
		this.incrementMessageCounter();
		this.resetProtractedDialogTimer();
		this.improveLinks(message);
		this.tryForward(message);
		this.improveMessageTag(message);
		this.writeLog(message);
	}

	Bridge.prototype.tryForward = function(message) {
		message.wasForwarded = false;
		if (message.author.name == 'stranger' && message.author.side == message.side) {
			if (this.chats[(message.side + 1) % 2].status() != 'connected') return;
			if (!window.cvtSettings.forwarding) return;
			if (message.body.search('!cvt_no_forward') != -1) return;
			if (window.cvtSettings.contactFilter && haveContacts(message.body)) return;
			message.wasForwarded = true;
			this.chats[(message.side + 1) % 2].sendMessage(message);
		}
	}

	// заменяет HTML код ссылки на саму ссылку, фикс #14
	Bridge.prototype.improveLinks = function(message) {
		message.body = message.body.replace(/<a href="([^"]*)"[^>]*>[^<]*<\/a>/g, "$1");
	}

	Bridge.prototype.improveMessageTag = function(message) {
		var author;
		var colorFix = '';
		if (message.author.name == 'stranger') {
			if (message.author.side == message.side) {
				author = 'Отсюда';
			} else {
				author = 'Сюда';
			}
		} else {
			author = 'Анон';
			colorFix = ' style="background-color:#0e0;"';
		}
		var elem = this.chats[message.side].frame.document.querySelector('#messages').firstChild.lastChild;
		var tag = elem.querySelector('.name');
		tag.innerHTML = '<i' + colorFix + '>' + author + '</i>';
		if (message.author.side == message.side && message.author.name == 'stranger') {
			var buttonColor = message.wasForwarded?'#aaa':'#f00';
			var button = $('<a href="#" style="color: ' + buttonColor + '; float: right;">отпр</a>')
			.appendTo(elem).click(function() {
				this.chats[(message.side + 1) % 2].sendMessage(message);
				button.css({'color':'#aaa'});
			}.bind(this));
		}
		var msg = elem.querySelector('.message');
		var subpictures = $(msg).find('img');
		if (subpictures.size() > 0) {
			var picture = subpictures.first();
			picture.siblings().hide();
			$('<a href="' + picture.attr('src') + '" target="_blank"></a>')
			.insertBefore(picture)
			.append(picture.detach());
		} else {
			this.deep_linkify(msg);
		}
	}
    
	Bridge.prototype.deep_linkify = function(node) {
		if (node.nodeType == node.TEXT_NODE) {
			var matchpos = node.data.search(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi);
			if (matchpos >= 0) {
				var matched = node.splitText(matchpos);
				var last = matched.splitText(RegExp.lastMatch.length);
				var href = /^https?:\/\//.test(matched.data) ? matched.data : "http://" + matched.data;
				$('<a href="' + href + '" target="_blank"></a>')
				.append(matched)
				.insertBefore(last);
			}
		} else if (node.tagName != "STYLE" && node.tagName != "SCRIPT" && node.tagName != "A") {
			var child;
			for (var i = 0; child = node.childNodes[i]; ++i) {
				this.deep_linkify(child);
			}
		}
	}

	Bridge.prototype.writeLog = function(message) {
		var author;
		var color;
		if (message.author.name == 'stranger') {
			if (message.author.side != message.side) return;
			author = 'Аутист ' + (message.author.side + 1);
			color = ['#f00', '#00f'][message.author.side]
		} else {
			author = 'Анон ' + (message.side + 1) + '-му аутисту';
			color = '#0f0';
		}
		this.log += '<span style="color: white; background-color:'
		+ color + ';">' + author + '</span> '
		+ message.body + '<br/>';

	}

	Bridge.prototype.showLog = function() {
		window.open('data:text/html;charset=utf-8,' + encodeURIComponent(this.log));
	}

	Bridge.prototype.onDisconnect = function(side, initiator) {
		this.clearProtractedDialogTimer();
		if (initiator != 'program') {
			if (window.cvtSettings.autoDisconnect)
				this.chats[(side + 1) % 2].stop('program');
		}
		if (window.cvtSettings.autoRestart) {
			setTimeout(function() {
				this.chats[side].start();
			}.bind(this), side * 400 + 400);
		}
		this.log += '<span style="background-color: #ccc;">Аутист ' + (side + 1) + ' отключился</span><br/>';
	}

	function Chat(side, frame, bridge) {
		this.side = side;
		this.frame = frame;
		this.bridge = bridge;
		this.status('disconnected');
		this.localMessages = [];
		this.messageQueue = [];
		this.inject();
	}

	Chat.prototype.status = function(newStatus) {
		if (newStatus !== undefined) {
			this._status = newStatus;
			$('#status' + this.side).text(newStatus);
		}
		return this._status;
	}

	Chat.prototype.inject = function() {
		this.frame.onmessageIn = function(evt) {
			var result = this.frame.onmessageOut(evt);
			this.processEvent(JSON.parse(evt.data));
			return result;
		}.bind(this);

		var onmessageSource = this.frame.WebSocketCreate.toString();
		onmessageSource = onmessageSource.replace("manSocket.onmessage =", 
			"manSocket.onmessage = function(evt) { console.log('a'); return onmessageIn(evt); }; onmessageOut =");

		injectToFrame("WebSocketCreate = " + onmessageSource, this.frame);

		var sendNewMessage = this.frame.sendNewMessage;
		this.frame.sendNewMessage = function() {
			this.reportLocalMessage(this.frame.$.trim(this.frame.$('#text').val()));
			sendNewMessage();
		}.bind(this);

		// слишком много оригинальный код мусорит в лог
		this.frame["console"]["log"] = function () {}

		// фикс #16
		// убираем переключение фокуса в оригинальном коде
		var functionsToDisableFocusBlink = [
			this.frame.startNewChat,
			this.frame.evalChatConnect,
			this.frame.take_snapshot,
			this.frame.my_completion_handler,
			this.frame.sendNewMessage
		];

		for (fn in functionsToDisableFocusBlink) {
			var code = fn.toString();
			fixedCode = code.replace(".focus()", "");
			injectToFrame(fixedCode, this.frame);
		}

		injectToFrame("infr = function infr(){return false;}", this.frame);
		injectToFrame("initBz1 = function initBz1(){ }", this.frame);
		injectToFrame("clearInterval(ImagesCheckTimer);", this.frame);
		injectToFrame("removeAllFiles = function removeAllFiles(){ }", this.frame);
	}

	Chat.prototype.processEvent = function(data) {
		if (data.action == 'message_from_user') {
			var body = data.message;
			var author;
			if (data.sender == 'someone') {
				author = new Author('stranger', this.side);
			} else if (this.checkLocalMessage(body)) {
				author = new Author('local', this.side);
			} else {
				author = new Author('stranger', (this.side + 1) % 2);
			}
			var message = new Message(author, this.side, body);
			this.onMessage(message);
		} else if (data.desc == 'chat_removed' || data.action == 'chat_removed') {
			this.onDisconnect();
		} else if (data.action == 'chat_connected') {
			this.onConnect();
		}
	}

	Chat.prototype.onMessage = function(message) {
		this.bridge.onMessage(message);
	}

	Chat.prototype.onDisconnect = function() {
		this.bridge.onDisconnect(this.side, this.stopInitiator || 'stranger');
		this.stopInitiator = undefined;
		this.status('disconnected');
	}

	Chat.prototype.onConnect = function() {
		clearTimeout(this.findingTimer);
		this.status('connected');
		this.bridge.onChatConnected(this);
		while (this.messageQueue.length > 0) {
			this.sendMessage(this.messageQueue.shift());
		}
	}

	Chat.prototype.checkLocalMessage = function(message) {
		var index = this.localMessages.indexOf(message);
		if (index != -1) {
			this.localMessages.splice(index, 1);
			return true;
		}
		return false;
	}

	Chat.prototype.reportLocalMessage = function(message) {
		this.localMessages.push(message);
	}

	Chat.prototype.sendMessage = function(message) {
		if (this.status() == 'finding') {
			this.messageQueue.push(message);
		}
		this.frame.sendSocketMessage({
			action: "send_message",
			uid: this.frame.ss_uid,
			chat: this.frame.ss_chat,
			message: message.body
		});
	}

	Chat.prototype.start = function() {
		if (this.status() == 'disconnected') {
			createCookie("bid", "", -1);
			createCookie("valar", "", -1);
			this.frame.startNewChat();
			this.status('finding');
			this.findingTimer = setTimeout(function() {
				log('Finding timeout, reloading frame');
				this.reloadFrame();
			}.bind(this), 10000);
		}
	}

	Chat.prototype.stop = function(initiator) {
		if (this.status() == 'connected') {
			this.frame.closeCurrentChat();
			this.stopInitiator = initiator;
			this.status('disconnecting');
		}
	}

	Chat.prototype.reloadFrame = function() {
		var oldStatus = this.status();
		if (this.status() != 'disconnected') {
			this.stop();
		}
		this.status('reloading');
		this.frame.onbeforeunload = function () {}
		this.frame.location.reload();
		setTimeout(function() {
			log('Inject after reloading');
			this.inject();
			this.status('disconnected');
			if (oldStatus != 'disconnected') {
				this.start();
			}
		}.bind(this), 2000);
	}


	function Author(name, side) {
		this.name = name;
		this.side = side;
	}

	function Message(author, side, body) {
		this.author = author;
		this.body = body;
		this.side = side;
	}

	function Settings() {
		this.schema = {
			forwarding: {
				type: "bool",
				defaultVal: true,
				ru: "Пересылать"
			},
			autoDisconnect: {
				type: 'bool',
				defaultVal: true,
				ru: 'Автодисконнект'
			},
			autoRestart: {
				type: 'bool',
				defaultVal: true,
				ru: 'Авторестарт'
			},
			restartProtractedDialogs: {
				type: 'bool',
				defaultVal: false,
				ru: 'Рестарт затянувшихся бесед'
			},
			contactFilter: {
				type: 'bool',
				defaultVal: false,
				ru: 'Фильтровать контакты'
			},
			debugLogging: {
				type: 'bool',
				defaultVal: false,
				ru: 'Отладка скрипта'
			}
		};

		this.load();
		this.initContorls();

		for (setting in this.schema) {
			if (this.schema[setting].type == 'bool')
				this.addToggle(setting);
		}
	}

	Settings.prototype.load = function(setting) {
		if (setting) {
			this[setting] = localStorage['cvt_' + setting];
			if (this[setting] == undefined)
				this[setting] = this.schema[setting].defaultVal;
			if (this.schema[setting].type == 'bool' && typeof this[setting] == 'string')
				this[setting] = this[setting] == 'true';
		} else {
			for (setting in this.schema) {
				this.load(setting);
			}
		}
	}

	Settings.prototype.save = function(setting) {
		if (setting) {
			localStorage['cvt_' + setting] = this[setting];
		} else {
			for (setting in this.schema) {
				this.save(setting);
			}
		}
	}

	Settings.prototype.initContorls = function() {
		style =
		'position: absolute;'+
		'top: 0; left:0;'+
		'width: 100%; height: 20px;'+
		'background-color: #fff;';
		this.controls = $('<div id="cvt_menu" style="' + style + '"><span id="status0"></span> | <span id="status1"></span></div>').appendTo(document.body);
	}

	Settings.prototype.addButton = function(caption, cb) {
		this.controls.append(' | ');
		return $('<a href="#">' + caption + '</a>').appendTo(this.controls).click(cb);
	}

	Settings.prototype.addToggle = function(setting) {
		var button = this.addButton('['+ (this[setting]?'x':'_') + ']' + this.schema[setting].ru, function() {
			this[setting] = !this[setting];
			button.text('['+ (this[setting]?'x':'_') + ']' + this.schema[setting].ru);
			this.save(setting);
		}.bind(this));
	}

});
